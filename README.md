`hows`
=====

...or the how(s) to make general purpose operations on AWS.
Well, the name also comes from the company I work for Hove: Hove+AWS = `hows`.

# Purpose
The `aws` CLI is proposing a tons of functionality. But do you remember how to
restart an ECS service? Or to edit a entry in Parameter Store? Or even watch
logs in CloudWatch?

`hows` make it easier to do all of these operations by looking into a set of
commands by using your own words. `hows` uses a subtle combination of
`skim/fzf`, `jq` and `aws` to simplify your workflow.
