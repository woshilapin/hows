#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

HELP="""
hows-ssm-put-parameter <aws_profile> <store_parameter_name> <value>

Create or update a store parameter <store_parameter_name> with <value> for AWS
profile <aws_profile>.

	<aws_profile>	Name of the AWS profile to use
	<store_parameter_name>	Name of the SSM Store Parameter entry
	<value>	value of the SSM Store Parameter
"""

function ssm-put-parameter {
	if [[ $# != 3 ]]
	then
		echo "${HELP}"
		exit 1
	fi
	AWS_PROFILE="${1}"
	STORE_PARAMETER_NAME="${2}"
	VALUE="${3}"
	aws ssm put-parameter \
		--profile "${AWS_PROFILE}" \
		--name "${STORE_PARAMETER_NAME}" \
		--value "${VALUE}" \
		--overwrite \
	| jq
}

# Do not proceed if this file is being `source`
(return 0 2>/dev/null) && return 0

ssm-put-parameter "${@}"
